﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace textileWarehouse
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void btnDailyProduce_Click(object sender, EventArgs e)
        {
            Form frm = new DailyProduce();
            frm.Show();
            Hide();
        }

        private void btnThreadStock_Click(object sender, EventArgs e)
        {
            Form frm = new ThreadStock();
            frm.Show();
            Hide();
        }

        private void btnClothStock_Click(object sender, EventArgs e)
        {
            Form frm = new ClothStock();
            frm.Show();
            Hide();
        }
    }
}
