﻿namespace textileWarehouse
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.btnThreadStock = new System.Windows.Forms.Button();
            this.btnClothStock = new System.Windows.Forms.Button();
            this.btnDailyProduce = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnThreadStock
            // 
            this.btnThreadStock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThreadStock.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnThreadStock.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnThreadStock.Location = new System.Drawing.Point(49, 230);
            this.btnThreadStock.Name = "btnThreadStock";
            this.btnThreadStock.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnThreadStock.Size = new System.Drawing.Size(149, 83);
            this.btnThreadStock.TabIndex = 1;
            this.btnThreadStock.Text = "موجودی نخ";
            this.btnThreadStock.UseVisualStyleBackColor = true;
            this.btnThreadStock.Click += new System.EventHandler(this.btnThreadStock_Click);
            // 
            // btnClothStock
            // 
            this.btnClothStock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClothStock.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnClothStock.Location = new System.Drawing.Point(49, 367);
            this.btnClothStock.Name = "btnClothStock";
            this.btnClothStock.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnClothStock.Size = new System.Drawing.Size(149, 90);
            this.btnClothStock.TabIndex = 2;
            this.btnClothStock.Text = "موجودی پارچه";
            this.btnClothStock.UseVisualStyleBackColor = true;
            this.btnClothStock.Click += new System.EventHandler(this.btnClothStock_Click);
            // 
            // btnDailyProduce
            // 
            this.btnDailyProduce.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDailyProduce.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnDailyProduce.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnDailyProduce.Location = new System.Drawing.Point(49, 89);
            this.btnDailyProduce.Name = "btnDailyProduce";
            this.btnDailyProduce.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnDailyProduce.Size = new System.Drawing.Size(149, 87);
            this.btnDailyProduce.TabIndex = 0;
            this.btnDailyProduce.Text = "تولید روزانه";
            this.btnDailyProduce.UseVisualStyleBackColor = true;
            this.btnDailyProduce.Click += new System.EventHandler(this.btnDailyProduce_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.ClientSize = new System.Drawing.Size(745, 500);
            this.Controls.Add(this.btnClothStock);
            this.Controls.Add(this.btnThreadStock);
            this.Controls.Add(this.btnDailyProduce);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnThreadStock;
        private System.Windows.Forms.Button btnClothStock;
        private System.Windows.Forms.Button btnDailyProduce;
    }
}

