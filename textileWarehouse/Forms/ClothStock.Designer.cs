﻿namespace textileWarehouse
{
    partial class ClothStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.machineStateGroup = new System.Windows.Forms.GroupBox();
            this.sumProduce = new System.Windows.Forms.Label();
            this.sumSend = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.sumFirstMachineLable = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Send = new System.Windows.Forms.Label();
            this.btnAddData = new System.Windows.Forms.Button();
            this.btnSum = new System.Windows.Forms.Button();
            this.btnPreviousPage = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnExit = new System.Windows.Forms.Button();
            this.dataGridViewSend = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewProdouce = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doneTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clothNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.threadType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textureType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnNextPage = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.machineStateGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProdouce)).BeginInit();
            this.SuspendLayout();
            // 
            // machineStateGroup
            // 
            this.machineStateGroup.Controls.Add(this.sumProduce);
            this.machineStateGroup.Controls.Add(this.sumSend);
            this.machineStateGroup.Controls.Add(this.label2);
            this.machineStateGroup.Controls.Add(this.sumFirstMachineLable);
            this.machineStateGroup.Controls.Add(this.label1);
            this.machineStateGroup.Controls.Add(this.Send);
            this.machineStateGroup.Controls.Add(this.btnAddData);
            this.machineStateGroup.Controls.Add(this.btnSum);
            this.machineStateGroup.Controls.Add(this.btnPreviousPage);
            this.machineStateGroup.Controls.Add(this.dateTimePicker1);
            this.machineStateGroup.Controls.Add(this.btnExit);
            this.machineStateGroup.Controls.Add(this.dataGridViewSend);
            this.machineStateGroup.Controls.Add(this.dataGridViewProdouce);
            this.machineStateGroup.Controls.Add(this.btnNextPage);
            this.machineStateGroup.Font = new System.Drawing.Font("IRANSans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.machineStateGroup.Location = new System.Drawing.Point(0, 0);
            this.machineStateGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.machineStateGroup.Name = "machineStateGroup";
            this.machineStateGroup.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.machineStateGroup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.machineStateGroup.Size = new System.Drawing.Size(1280, 814);
            this.machineStateGroup.TabIndex = 0;
            this.machineStateGroup.TabStop = false;
            this.machineStateGroup.Text = "ماشین شماره 1";
            // 
            // sumProduce
            // 
            this.sumProduce.AutoSize = true;
            this.sumProduce.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sumProduce.Location = new System.Drawing.Point(973, 24);
            this.sumProduce.Name = "sumProduce";
            this.sumProduce.Size = new System.Drawing.Size(82, 21);
            this.sumProduce.TabIndex = 15;
            this.sumProduce.Text = "sumProduce";
            this.sumProduce.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sumSend
            // 
            this.sumSend.AutoSize = true;
            this.sumSend.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sumSend.Location = new System.Drawing.Point(744, 24);
            this.sumSend.Name = "sumSend";
            this.sumSend.Size = new System.Drawing.Size(64, 21);
            this.sumSend.TabIndex = 14;
            this.sumSend.Text = "sumSend";
            this.sumSend.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(1061, 24);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(67, 21);
            this.label2.TabIndex = 13;
            this.label2.Text = "جمع تولید:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sumFirstMachineLable
            // 
            this.sumFirstMachineLable.AutoSize = true;
            this.sumFirstMachineLable.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sumFirstMachineLable.Location = new System.Drawing.Point(814, 24);
            this.sumFirstMachineLable.Name = "sumFirstMachineLable";
            this.sumFirstMachineLable.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.sumFirstMachineLable.Size = new System.Drawing.Size(69, 21);
            this.sumFirstMachineLable.TabIndex = 12;
            this.sumFirstMachineLable.Text = "جمع ارسال:";
            this.sumFirstMachineLable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(1232, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 21);
            this.label1.TabIndex = 11;
            this.label1.Text = "تولید";
            // 
            // Send
            // 
            this.Send.AutoSize = true;
            this.Send.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Send.Location = new System.Drawing.Point(628, 31);
            this.Send.Name = "Send";
            this.Send.Size = new System.Drawing.Size(38, 21);
            this.Send.TabIndex = 10;
            this.Send.Text = "ارسال";
            // 
            // btnAddData
            // 
            this.btnAddData.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnAddData.Location = new System.Drawing.Point(485, 14);
            this.btnAddData.Name = "btnAddData";
            this.btnAddData.Size = new System.Drawing.Size(99, 31);
            this.btnAddData.TabIndex = 9;
            this.btnAddData.Text = "اضافه کردن داده";
            this.btnAddData.UseVisualStyleBackColor = true;
            this.btnAddData.Click += new System.EventHandler(this.btnAddData_Click);
            // 
            // btnSum
            // 
            this.btnSum.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnSum.Location = new System.Drawing.Point(349, 14);
            this.btnSum.Name = "btnSum";
            this.btnSum.Size = new System.Drawing.Size(130, 31);
            this.btnSum.TabIndex = 8;
            this.btnSum.Text = "جمع تولید و ارسال";
            this.btnSum.UseVisualStyleBackColor = true;
            this.btnSum.Click += new System.EventHandler(this.btnSum_Click);
            // 
            // btnPreviousPage
            // 
            this.btnPreviousPage.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnPreviousPage.Location = new System.Drawing.Point(271, 13);
            this.btnPreviousPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPreviousPage.Name = "btnPreviousPage";
            this.btnPreviousPage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnPreviousPage.Size = new System.Drawing.Size(33, 32);
            this.btnPreviousPage.TabIndex = 7;
            this.btnPreviousPage.Text = ">";
            this.btnPreviousPage.UseVisualStyleBackColor = true;
            this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dateTimePicker1.Location = new System.Drawing.Point(6, 12);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 28);
            this.dateTimePicker1.TabIndex = 6;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExit.Location = new System.Drawing.Point(310, 13);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnExit.Size = new System.Drawing.Size(33, 32);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "x";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // dataGridViewSend
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dataGridViewSend.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewSend.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSend.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.description});
            this.dataGridViewSend.Location = new System.Drawing.Point(6, 56);
            this.dataGridViewSend.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridViewSend.MultiSelect = false;
            this.dataGridViewSend.Name = "dataGridViewSend";
            this.dataGridViewSend.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dataGridViewSend.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewSend.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSend.Size = new System.Drawing.Size(660, 750);
            this.dataGridViewSend.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "کد";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 53;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "date";
            this.dataGridViewTextBoxColumn2.HeaderText = "تاریخ";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 71;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "amount";
            this.dataGridViewTextBoxColumn3.HeaderText = "تعداد";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "threadType";
            this.dataGridViewTextBoxColumn4.HeaderText = "نوع نخ";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 82;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "textureType";
            this.dataGridViewTextBoxColumn5.HeaderText = "نوع بافت";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 96;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "weight";
            this.dataGridViewTextBoxColumn6.HeaderText = "وزن";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 62;
            // 
            // description
            // 
            this.description.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.description.HeaderText = "شرح";
            this.description.Name = "description";
            // 
            // dataGridViewProdouce
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dataGridViewProdouce.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewProdouce.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProdouce.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.doneTime,
            this.clothNumber,
            this.threadType,
            this.textureType,
            this.weight});
            this.dataGridViewProdouce.Location = new System.Drawing.Point(672, 56);
            this.dataGridViewProdouce.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridViewProdouce.MultiSelect = false;
            this.dataGridViewProdouce.Name = "dataGridViewProdouce";
            this.dataGridViewProdouce.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dataGridViewProdouce.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewProdouce.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewProdouce.Size = new System.Drawing.Size(602, 750);
            this.dataGridViewProdouce.TabIndex = 3;
            // 
            // id
            // 
            this.id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "کد";
            this.id.Name = "id";
            this.id.Width = 53;
            // 
            // doneTime
            // 
            this.doneTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.doneTime.DataPropertyName = "date";
            this.doneTime.HeaderText = "تاریخ";
            this.doneTime.Name = "doneTime";
            // 
            // clothNumber
            // 
            this.clothNumber.DataPropertyName = "amount";
            this.clothNumber.HeaderText = "تعداد";
            this.clothNumber.Name = "clothNumber";
            // 
            // threadType
            // 
            this.threadType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.threadType.DataPropertyName = "threadType";
            this.threadType.HeaderText = "نوع نخ";
            this.threadType.Name = "threadType";
            this.threadType.Width = 82;
            // 
            // textureType
            // 
            this.textureType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.textureType.DataPropertyName = "textureType";
            this.textureType.HeaderText = "نوع بافت";
            this.textureType.Name = "textureType";
            this.textureType.Width = 96;
            // 
            // weight
            // 
            this.weight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.weight.DataPropertyName = "weight";
            this.weight.HeaderText = "وزن";
            this.weight.Name = "weight";
            this.weight.Width = 62;
            // 
            // btnNextPage
            // 
            this.btnNextPage.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnNextPage.Location = new System.Drawing.Point(232, 13);
            this.btnNextPage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNextPage.Name = "btnNextPage";
            this.btnNextPage.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnNextPage.Size = new System.Drawing.Size(33, 32);
            this.btnNextPage.TabIndex = 1;
            this.btnNextPage.Text = "<";
            this.btnNextPage.UseVisualStyleBackColor = true;
            this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
            // 
            // ClothStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 718);
            this.Controls.Add(this.machineStateGroup);
            this.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ClothStock";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "موجودی پارچه";
            this.Load += new System.EventHandler(this.ClothStock_Load);
            this.machineStateGroup.ResumeLayout(false);
            this.machineStateGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProdouce)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox machineStateGroup;
        private System.Windows.Forms.Button btnNextPage;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataGridView dataGridViewProdouce;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridView dataGridViewSend;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button btnPreviousPage;
        private System.Windows.Forms.Button btnSum;
        private System.Windows.Forms.Button btnAddData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Send;
        public System.Windows.Forms.Label sumProduce;
        public System.Windows.Forms.Label sumSend;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label sumFirstMachineLable;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn doneTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn clothNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn threadType;
        private System.Windows.Forms.DataGridViewTextBoxColumn textureType;
        private System.Windows.Forms.DataGridViewTextBoxColumn weight;
    }
}