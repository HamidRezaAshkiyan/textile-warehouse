﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace textileWarehouse
{
    public partial class ThreadStock : Form
    {
        public ThreadStock()
        {
            InitializeComponent();
        }

        private void ThreadStock_Load(object sender, EventArgs e)
        {
            Thread T = new Thread();
            Cloth C = new Cloth();
            DateTime DT = new DateTime();
            DT = dateTimePicker1.Value;

            dataGridViewRecieve.DataSource = T.SelectYearThread(false, DT);
            dataGridViewSent.DataSource = T.SelectYearThread(true, DT);
            dataGridViewSum.DataSource = C.SumAllMachineThread(DT);
        }

        private void btnAddData_Click(object sender, EventArgs e)
        {
            ThreadData frm = new ThreadData();
            frm.Show();
            Hide();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Main frm = new Main();
            frm.Show();
            Hide();
        }

        private void btnEditRecive_Click(object sender, EventArgs e)
        {
            Thread T = new Thread();
            DataTable dt = new DataTable();
            ThreadData frm = new ThreadData(dateTimePicker1, "ویرایش", false);

            string d = dataGridViewRecieve[0, dataGridViewRecieve.CurrentRow.Index].Value.ToString(); // for getting id
            dt = T.SelectOneThread(int.Parse(d));

            frm.txtDoneTime.Value = DateTime.Parse(dt.Rows[0]["time"].ToString());
            frm.sendStateFlag = bool.Parse(dt.Rows[0]["sendState"].ToString());
            frm.txtThreadType.Text = dt.Rows[0]["threadType"].ToString();
            frm.txtWeight.Text = dt.Rows[0]["weight"].ToString();
            frm.txtAmount.Text = dt.Rows[0]["amount"].ToString();
            frm.txtRSPerson.Text = dt.Rows[0]["person"].ToString();
            frm.txtPrice.Text = dt.Rows[0]["price"].ToString();
            frm.txtId.Text = dt.Rows[0]["id"].ToString();
            frm.txtId.ReadOnly = true;
            frm.txtId.Visible = true;
            frm.id.Visible = true;

            frm.Show();
            Hide();
        }
        
        private void btnEditSend_Click(object sender, EventArgs e)
        {
            Thread T = new Thread();
            DataTable dt = new DataTable();
            ThreadData frm = new ThreadData(dateTimePicker1, "ویرایش", true);

            string d = dataGridViewSent[0, dataGridViewSent.CurrentRow.Index].Value.ToString(); // for getting id
            dt = T.SelectOneThread(int.Parse(d));

            frm.txtDoneTime.Value = DateTime.Parse(dt.Rows[0]["time"].ToString());
            frm.sendStateFlag = bool.Parse(dt.Rows[0]["sendState"].ToString());
            frm.txtThreadType.Text = dt.Rows[0]["threadType"].ToString();
            frm.txtWeight.Text = dt.Rows[0]["weight"].ToString();
            frm.txtAmount.Text = dt.Rows[0]["amount"].ToString();
            frm.txtRSPerson.Text = dt.Rows[0]["person"].ToString();
            frm.txtPrice.Text = dt.Rows[0]["price"].ToString();
            frm.txtId.Text = dt.Rows[0]["id"].ToString();
            frm.txtId.ReadOnly = true;
            frm.txtId.Visible = true;
            frm.id.Visible = true;

            frm.Show();
            Hide();
        }

        private void btnThreadSum_Click(object sender, EventArgs e)
        {
            SumAllThread frm = new SumAllThread();
            frm.Show();
            Hide();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e) => ThreadStock_Load(null, null);
    }
}
