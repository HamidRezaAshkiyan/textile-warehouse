﻿using System;
using System.Windows.Forms;
using BL;

namespace textileWarehouse
{
    public partial class AddThreadData : Form
    {
        public bool sendStateFlag;
        public AddThreadData(bool state)
        {
            InitializeComponent();
            txtDoneTime.Value = dateTimePicker1.Value;
            sendStateFlag = state;
        }

        public AddThreadData(string v, bool state)
        {
            InitializeComponent();
            if (v == "ویرایش")
                btnSubmit.Text = v;
            txtDoneTime.Value = dateTimePicker1.Value;
            sendStateFlag = state;
        }

        private void AddData_Load(object sender, EventArgs e)
        {
            if (sendStateFlag == true)
                radioButtonSend.Checked = true;
            else
                radioButtonProdouce.Checked = true;

            txtDoneTime.Format = DateTimePickerFormat.Time;
            txtDoneTime.ShowUpDown = true;
            txtDoneTime.Value = dateTimePicker1.Value;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (radioButtonProdouce.Checked == true || radioButtonSend.Checked == true)
            {
                if (radioButtonProdouce.Checked == true)
                {
                    sendStateFlag = false;
                }
                else 
                    sendStateFlag = true;
            }
            else
            {
                MessageBox.Show("لطفا اطلاعات را کامل کنید");
                AddData_Load(null, null);
            }

            Cloth C = new Cloth()
            {
                textureType = txtTextureType.Text,
                threadType = txtThreadType.Text,
                weight = float.Parse(txtWeight.Text),
                machineNumber = int.Parse(txtMachineNumber.Text),
                doneTime = txtDoneTime.Value,
                sendState = sendStateFlag,
                amount = int.Parse(txtAmount.Text),
            };
            
            if (txtId.Visible == true)
            {
                C.Update(int.Parse(txtId.Text));
                MessageBox.Show("اطلاعات با موفقیت به روزرسانی شد.");
                DailyProduce da = new DailyProduce();
                da.Show();
                Hide();
            }
            else
            {
                C.Add();
                MessageBox.Show("اطلاعات با موفقیت ذخیره شد.");
                var dialogResult = MessageBox.Show("ایا تمایل به اضافه کردن داده دیگری دارید؟", "هشدار", MessageBoxButtons.YesNoCancel);
                if (dialogResult == DialogResult.Yes)
                {
                    //AddThreadData frm = new AddThreadData(sendStateFlag);
                    //frm.Show();
                    //Hide();
                    AddData_Load(null, null);
                }
                else
                {
                    DailyProduce da = new DailyProduce();
                    da.Show();
                    Hide();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DailyProduce da = new DailyProduce();
            da.Show();
            Hide();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            AddData_Load(null, null);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtId.Visible == true)
            {
                Cloth C = new Cloth();
                var dialogResult = MessageBox.Show("مطمئنید؟", "هشدار", MessageBoxButtons.YesNoCancel);
                if (dialogResult == DialogResult.Yes)
                {
                    C.DeleteCloth(int.Parse(txtId.Text));
                    MessageBox.Show("داده با موفقیت حذف شد");
                    DailyProduce da = new DailyProduce();
                    da.Show();
                    Hide();
                }
            }
        }
    }
}
