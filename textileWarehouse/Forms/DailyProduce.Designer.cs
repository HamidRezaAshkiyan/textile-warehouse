﻿namespace textileWarehouse
{
    partial class DailyProduce
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAddMachine = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.Machines = new System.Windows.Forms.GroupBox();
            this.dataGridViewSum = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.threadType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textureType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doneTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAddData = new System.Windows.Forms.Button();
            this.btnNextPage = new System.Windows.Forms.Button();
            this.btnPreviousPage = new System.Windows.Forms.Button();
            this.btnEditMachineOne = new System.Windows.Forms.Button();
            this.btnEditMachineTwo = new System.Windows.Forms.Button();
            this.sumFirstMachineLable = new System.Windows.Forms.Label();
            this.sumSecondMachineLable = new System.Windows.Forms.Label();
            this.sumFirstMachine = new System.Windows.Forms.Label();
            this.sumSecondMachine = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.Machines.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddMachine
            // 
            this.btnAddMachine.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnAddMachine.Location = new System.Drawing.Point(1055, 8);
            this.btnAddMachine.Name = "btnAddMachine";
            this.btnAddMachine.Size = new System.Drawing.Size(142, 39);
            this.btnAddMachine.TabIndex = 0;
            this.btnAddMachine.Text = "اضافه کردن ماشین";
            this.btnAddMachine.UseVisualStyleBackColor = true;
            this.btnAddMachine.Click += new System.EventHandler(this.btnAddMachine_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dateTimePicker1.Location = new System.Drawing.Point(18, 8);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 28);
            this.dateTimePicker1.TabIndex = 0;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // Machines
            // 
            this.Machines.Controls.Add(this.dataGridViewSum);
            this.Machines.Controls.Add(this.dataGridView2);
            this.Machines.Controls.Add(this.dataGridView1);
            this.Machines.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Machines.Location = new System.Drawing.Point(12, 50);
            this.Machines.Name = "Machines";
            this.Machines.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Machines.Size = new System.Drawing.Size(1185, 528);
            this.Machines.TabIndex = 1;
            this.Machines.TabStop = false;
            this.Machines.Text = " ماشین ها";
            // 
            // dataGridViewSum
            // 
            this.dataGridViewSum.AllowUserToDeleteRows = false;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dataGridViewSum.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewSum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSum.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn9});
            this.dataGridViewSum.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewSum.MultiSelect = false;
            this.dataGridViewSum.Name = "dataGridViewSum";
            this.dataGridViewSum.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dataGridViewSum.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewSum.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSum.Size = new System.Drawing.Size(337, 503);
            this.dataGridViewSum.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn7.DataPropertyName = "threadType";
            this.dataGridViewTextBoxColumn7.HeaderText = "نوع نخ";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 62;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn9.DataPropertyName = "weight";
            dataGridViewCellStyle16.Format = "N2";
            dataGridViewCellStyle16.NullValue = null;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle16;
            this.dataGridViewTextBoxColumn9.HeaderText = "وزن";
            this.dataGridViewTextBoxColumn9.MaxInputLength = 6;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridView2
            // 
            dataGridViewCellStyle18.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dataGridView2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dataGridView2.Location = new System.Drawing.Point(349, 19);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dataGridView2.RowsDefaultCellStyle = dataGridViewCellStyle19;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(408, 503);
            this.dataGridView2.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "کد";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 44;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "threadType";
            this.dataGridViewTextBoxColumn2.HeaderText = "نوع نخ";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 48;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "textureType";
            this.dataGridViewTextBoxColumn3.HeaderText = "نوع بافت";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 67;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "weight";
            this.dataGridViewTextBoxColumn4.HeaderText = "وزن";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 50;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "doneTime";
            this.dataGridViewTextBoxColumn5.HeaderText = "ساعت تولید";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridView1
            // 
            dataGridViewCellStyle20.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle20;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.threadType,
            this.textureType,
            this.weight,
            this.doneTime});
            this.dataGridView1.Location = new System.Drawing.Point(763, 19);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(416, 503);
            this.dataGridView1.TabIndex = 0;
            // 
            // id
            // 
            this.id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "کد";
            this.id.Name = "id";
            this.id.Width = 44;
            // 
            // threadType
            // 
            this.threadType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.threadType.DataPropertyName = "threadType";
            this.threadType.HeaderText = "نوع نخ";
            this.threadType.Name = "threadType";
            this.threadType.Width = 48;
            // 
            // textureType
            // 
            this.textureType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.textureType.DataPropertyName = "textureType";
            this.textureType.HeaderText = "نوع بافت";
            this.textureType.Name = "textureType";
            this.textureType.Width = 67;
            // 
            // weight
            // 
            this.weight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.weight.DataPropertyName = "weight";
            this.weight.HeaderText = "وزن";
            this.weight.Name = "weight";
            this.weight.Width = 50;
            // 
            // doneTime
            // 
            this.doneTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.doneTime.DataPropertyName = "doneTime";
            this.doneTime.HeaderText = "ساعت تولید";
            this.doneTime.Name = "doneTime";
            // 
            // btnAddData
            // 
            this.btnAddData.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnAddData.Location = new System.Drawing.Point(939, 8);
            this.btnAddData.Name = "btnAddData";
            this.btnAddData.Size = new System.Drawing.Size(110, 39);
            this.btnAddData.TabIndex = 2;
            this.btnAddData.Text = "اضافه کردن داده";
            this.btnAddData.UseVisualStyleBackColor = true;
            this.btnAddData.Click += new System.EventHandler(this.btnAddData_Click);
            // 
            // btnNextPage
            // 
            this.btnNextPage.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnNextPage.Location = new System.Drawing.Point(255, 8);
            this.btnNextPage.Name = "btnNextPage";
            this.btnNextPage.Size = new System.Drawing.Size(74, 28);
            this.btnNextPage.TabIndex = 3;
            this.btnNextPage.Text = "صفحه بعد";
            this.btnNextPage.UseVisualStyleBackColor = true;
            this.btnNextPage.Click += new System.EventHandler(this.btnNextPage_Click);
            // 
            // btnPreviousPage
            // 
            this.btnPreviousPage.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnPreviousPage.Location = new System.Drawing.Point(335, 8);
            this.btnPreviousPage.Name = "btnPreviousPage";
            this.btnPreviousPage.Size = new System.Drawing.Size(74, 28);
            this.btnPreviousPage.TabIndex = 4;
            this.btnPreviousPage.Text = "صفحه قبل";
            this.btnPreviousPage.UseVisualStyleBackColor = true;
            this.btnPreviousPage.Click += new System.EventHandler(this.btnPreviousPage_Click);
            // 
            // btnEditMachineOne
            // 
            this.btnEditMachineOne.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnEditMachineOne.Location = new System.Drawing.Point(814, 8);
            this.btnEditMachineOne.Name = "btnEditMachineOne";
            this.btnEditMachineOne.Size = new System.Drawing.Size(119, 39);
            this.btnEditMachineOne.TabIndex = 6;
            this.btnEditMachineOne.Text = "ویرایش ماشین 1";
            this.btnEditMachineOne.UseVisualStyleBackColor = true;
            this.btnEditMachineOne.Click += new System.EventHandler(this.btnEditMachineOne_Click);
            // 
            // btnEditMachineTwo
            // 
            this.btnEditMachineTwo.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnEditMachineTwo.Location = new System.Drawing.Point(689, 8);
            this.btnEditMachineTwo.Name = "btnEditMachineTwo";
            this.btnEditMachineTwo.Size = new System.Drawing.Size(119, 39);
            this.btnEditMachineTwo.TabIndex = 7;
            this.btnEditMachineTwo.Text = "ویرایش ماشین 2";
            this.btnEditMachineTwo.UseVisualStyleBackColor = true;
            this.btnEditMachineTwo.Click += new System.EventHandler(this.btnEditMachineTwo_Click);
            // 
            // sumFirstMachineLable
            // 
            this.sumFirstMachineLable.AutoSize = true;
            this.sumFirstMachineLable.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sumFirstMachineLable.Location = new System.Drawing.Point(470, 9);
            this.sumFirstMachineLable.Name = "sumFirstMachineLable";
            this.sumFirstMachineLable.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.sumFirstMachineLable.Size = new System.Drawing.Size(117, 21);
            this.sumFirstMachineLable.TabIndex = 8;
            this.sumFirstMachineLable.Text = "جمع تولید ماشین 1:";
            this.sumFirstMachineLable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sumSecondMachineLable
            // 
            this.sumSecondMachineLable.AutoSize = true;
            this.sumSecondMachineLable.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sumSecondMachineLable.Location = new System.Drawing.Point(470, 30);
            this.sumSecondMachineLable.Name = "sumSecondMachineLable";
            this.sumSecondMachineLable.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.sumSecondMachineLable.Size = new System.Drawing.Size(117, 21);
            this.sumSecondMachineLable.TabIndex = 9;
            this.sumSecondMachineLable.Text = "جمع تولید ماشین 2:";
            this.sumSecondMachineLable.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sumFirstMachine
            // 
            this.sumFirstMachine.AutoSize = true;
            this.sumFirstMachine.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sumFirstMachine.Location = new System.Drawing.Point(427, 9);
            this.sumFirstMachine.Name = "sumFirstMachine";
            this.sumFirstMachine.Size = new System.Drawing.Size(36, 21);
            this.sumFirstMachine.TabIndex = 10;
            this.sumFirstMachine.Text = "SFM";
            this.sumFirstMachine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sumSecondMachine
            // 
            this.sumSecondMachine.AutoSize = true;
            this.sumSecondMachine.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.sumSecondMachine.Location = new System.Drawing.Point(427, 30);
            this.sumSecondMachine.Name = "sumSecondMachine";
            this.sumSecondMachine.Size = new System.Drawing.Size(37, 21);
            this.sumSecondMachine.TabIndex = 11;
            this.sumSecondMachine.Text = "SSM";
            this.sumSecondMachine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnExit.Location = new System.Drawing.Point(224, 8);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnExit.Size = new System.Drawing.Size(25, 27);
            this.btnExit.TabIndex = 12;
            this.btnExit.Text = "x";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // DailyProduce
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1221, 585);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.sumSecondMachine);
            this.Controls.Add(this.sumFirstMachine);
            this.Controls.Add(this.sumSecondMachineLable);
            this.Controls.Add(this.sumFirstMachineLable);
            this.Controls.Add(this.btnEditMachineTwo);
            this.Controls.Add(this.btnEditMachineOne);
            this.Controls.Add(this.btnPreviousPage);
            this.Controls.Add(this.btnNextPage);
            this.Controls.Add(this.btnAddData);
            this.Controls.Add(this.Machines);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btnAddMachine);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DailyProduce";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تولید روزانه";
            this.Load += new System.EventHandler(this.DailyProduce_Load);
            this.Machines.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddMachine;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox Machines;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnAddData;
        private System.Windows.Forms.Button btnNextPage;
        private System.Windows.Forms.Button btnPreviousPage;
        private System.Windows.Forms.Button btnEditMachineOne;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn threadType;
        private System.Windows.Forms.DataGridViewTextBoxColumn textureType;
        private System.Windows.Forms.DataGridViewTextBoxColumn weight;
        private System.Windows.Forms.DataGridViewTextBoxColumn doneTime;
        private System.Windows.Forms.Button btnEditMachineTwo;
        private System.Windows.Forms.DataGridView dataGridViewSum;
        public System.Windows.Forms.Label sumFirstMachineLable;
        public System.Windows.Forms.Label sumSecondMachineLable;
        public System.Windows.Forms.Label sumFirstMachine;
        public System.Windows.Forms.Label sumSecondMachine;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
    }
}