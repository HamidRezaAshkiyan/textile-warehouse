﻿namespace textileWarehouse
{
    partial class ThreadData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButtonProdouce = new System.Windows.Forms.RadioButton();
            this.radioButtonSend = new System.Windows.Forms.RadioButton();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.amount = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtId = new System.Windows.Forms.TextBox();
            this.id = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtDoneTime = new System.Windows.Forms.DateTimePicker();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.weight = new System.Windows.Forms.Label();
            this.doneTime = new System.Windows.Forms.Label();
            this.txtThreadType = new System.Windows.Forms.TextBox();
            this.threadType = new System.Windows.Forms.Label();
            this.txtRSPerson = new System.Windows.Forms.TextBox();
            this.RSPerson = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.price = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // radioButtonProdouce
            // 
            this.radioButtonProdouce.AutoSize = true;
            this.radioButtonProdouce.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.radioButtonProdouce.Location = new System.Drawing.Point(89, 385);
            this.radioButtonProdouce.Name = "radioButtonProdouce";
            this.radioButtonProdouce.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButtonProdouce.Size = new System.Drawing.Size(66, 25);
            this.radioButtonProdouce.TabIndex = 7;
            this.radioButtonProdouce.TabStop = true;
            this.radioButtonProdouce.Text = "دریافت";
            this.radioButtonProdouce.UseVisualStyleBackColor = true;
            // 
            // radioButtonSend
            // 
            this.radioButtonSend.AutoSize = true;
            this.radioButtonSend.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.radioButtonSend.Location = new System.Drawing.Point(186, 385);
            this.radioButtonSend.Name = "radioButtonSend";
            this.radioButtonSend.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButtonSend.Size = new System.Drawing.Size(56, 25);
            this.radioButtonSend.TabIndex = 6;
            this.radioButtonSend.TabStop = true;
            this.radioButtonSend.Text = "ارسال";
            this.radioButtonSend.UseVisualStyleBackColor = true;
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtAmount.Location = new System.Drawing.Point(42, 112);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(200, 25);
            this.txtAmount.TabIndex = 0;
            // 
            // amount
            // 
            this.amount.AutoSize = true;
            this.amount.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.amount.Location = new System.Drawing.Point(285, 115);
            this.amount.Name = "amount";
            this.amount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.amount.Size = new System.Drawing.Size(37, 21);
            this.amount.TabIndex = 33;
            this.amount.Text = "تعداد";
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnCancel.Location = new System.Drawing.Point(42, 429);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 28);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "انصراف";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtId
            // 
            this.txtId.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtId.Location = new System.Drawing.Point(42, 342);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(200, 25);
            this.txtId.TabIndex = 5;
            this.txtId.Visible = false;
            // 
            // id
            // 
            this.id.AutoSize = true;
            this.id.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.id.Location = new System.Drawing.Point(285, 345);
            this.id.Name = "id";
            this.id.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.id.Size = new System.Drawing.Size(23, 21);
            this.id.TabIndex = 30;
            this.id.Text = "کد";
            this.id.Visible = false;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnSubmit.Location = new System.Drawing.Point(159, 429);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(83, 28);
            this.btnSubmit.TabIndex = 8;
            this.btnSubmit.Text = "ثبت";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // txtDoneTime
            // 
            this.txtDoneTime.CalendarFont = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtDoneTime.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtDoneTime.Location = new System.Drawing.Point(42, 17);
            this.txtDoneTime.Name = "txtDoneTime";
            this.txtDoneTime.Size = new System.Drawing.Size(200, 28);
            this.txtDoneTime.TabIndex = 10;
            // 
            // txtWeight
            // 
            this.txtWeight.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtWeight.Location = new System.Drawing.Point(42, 250);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(200, 25);
            this.txtWeight.TabIndex = 3;
            // 
            // weight
            // 
            this.weight.AutoSize = true;
            this.weight.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.weight.Location = new System.Drawing.Point(285, 255);
            this.weight.Name = "weight";
            this.weight.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.weight.Size = new System.Drawing.Size(30, 21);
            this.weight.TabIndex = 25;
            this.weight.Text = "وزن";
            // 
            // doneTime
            // 
            this.doneTime.AutoSize = true;
            this.doneTime.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.doneTime.Location = new System.Drawing.Point(280, 22);
            this.doneTime.Name = "doneTime";
            this.doneTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.doneTime.Size = new System.Drawing.Size(64, 21);
            this.doneTime.TabIndex = 27;
            this.doneTime.Text = "زمان تولید";
            // 
            // txtThreadType
            // 
            this.txtThreadType.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtThreadType.Location = new System.Drawing.Point(42, 158);
            this.txtThreadType.Name = "txtThreadType";
            this.txtThreadType.Size = new System.Drawing.Size(200, 25);
            this.txtThreadType.TabIndex = 1;
            this.txtThreadType.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // threadType
            // 
            this.threadType.AutoSize = true;
            this.threadType.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.threadType.Location = new System.Drawing.Point(285, 160);
            this.threadType.Name = "threadType";
            this.threadType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.threadType.Size = new System.Drawing.Size(47, 21);
            this.threadType.TabIndex = 23;
            this.threadType.Text = "نوع نخ";
            // 
            // txtRSPerson
            // 
            this.txtRSPerson.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtRSPerson.Location = new System.Drawing.Point(42, 296);
            this.txtRSPerson.Name = "txtRSPerson";
            this.txtRSPerson.Size = new System.Drawing.Size(200, 25);
            this.txtRSPerson.TabIndex = 4;
            this.txtRSPerson.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RSPerson
            // 
            this.RSPerson.AutoSize = true;
            this.RSPerson.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.RSPerson.Location = new System.Drawing.Point(285, 300);
            this.RSPerson.Name = "RSPerson";
            this.RSPerson.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RSPerson.Size = new System.Drawing.Size(97, 21);
            this.RSPerson.TabIndex = 37;
            this.RSPerson.Text = "فرستنده / گیرنده";
            // 
            // txtPrice
            // 
            this.txtPrice.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtPrice.Location = new System.Drawing.Point(42, 204);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(200, 25);
            this.txtPrice.TabIndex = 2;
            this.txtPrice.Text = "0";
            // 
            // price
            // 
            this.price.AutoSize = true;
            this.price.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.price.Location = new System.Drawing.Point(285, 210);
            this.price.Name = "price";
            this.price.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.price.Size = new System.Drawing.Size(40, 21);
            this.price.TabIndex = 39;
            this.price.Text = "قیمت";
            // 
            // txtDescription
            // 
            this.txtDescription.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtDescription.Location = new System.Drawing.Point(42, 66);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(200, 25);
            this.txtDescription.TabIndex = 40;
            this.txtDescription.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.Location = new System.Drawing.Point(285, 68);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(34, 21);
            this.label1.TabIndex = 41;
            this.label1.Text = "شرح";
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnDelete.Location = new System.Drawing.Point(286, 429);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(83, 28);
            this.btnDelete.TabIndex = 42;
            this.btnDelete.Text = "حذف";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // ThreadData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 470);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPrice);
            this.Controls.Add(this.price);
            this.Controls.Add(this.txtRSPerson);
            this.Controls.Add(this.RSPerson);
            this.Controls.Add(this.radioButtonProdouce);
            this.Controls.Add(this.radioButtonSend);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.amount);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.id);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtDoneTime);
            this.Controls.Add(this.txtWeight);
            this.Controls.Add(this.weight);
            this.Controls.Add(this.doneTime);
            this.Controls.Add(this.txtThreadType);
            this.Controls.Add(this.threadType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ThreadData";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "form";
            this.Load += new System.EventHandler(this.ThreadData_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonProdouce;
        private System.Windows.Forms.RadioButton radioButtonSend;
        public System.Windows.Forms.TextBox txtAmount;
        public System.Windows.Forms.Label amount;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.TextBox txtId;
        public System.Windows.Forms.Label id;
        public System.Windows.Forms.Button btnSubmit;
        public System.Windows.Forms.DateTimePicker txtDoneTime;
        public System.Windows.Forms.TextBox txtWeight;
        public System.Windows.Forms.Label weight;
        public System.Windows.Forms.Label doneTime;
        public System.Windows.Forms.TextBox txtThreadType;
        public System.Windows.Forms.Label threadType;
        public System.Windows.Forms.TextBox txtRSPerson;
        public System.Windows.Forms.Label RSPerson;
        public System.Windows.Forms.TextBox txtPrice;
        public System.Windows.Forms.Label price;
        public System.Windows.Forms.TextBox txtDescription;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnDelete;
    }
}