﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace textileWarehouse
{
    public partial class ThreadData : Form
    {
        public bool sendStateFlag;
        public ThreadData()
        {
            InitializeComponent();
        }
        public ThreadData(DateTimePicker date, bool state)
        {
            InitializeComponent();
            txtDoneTime.Value = date.Value;
            sendStateFlag = state;
        }

        public ThreadData(DateTimePicker date, string v, bool state)
        {
            InitializeComponent();
            if (v == "ویرایش")
                btnSubmit.Text = v;
            txtDoneTime.Value = date.Value;
            sendStateFlag = state;
        }

        private void ThreadData_Load(object sender, EventArgs e)
        {
            if (sendStateFlag == true)
                radioButtonSend.Checked = true;
            else
                radioButtonProdouce.Checked = true;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (radioButtonProdouce.Checked == true || radioButtonSend.Checked == true)
            {
                if (radioButtonProdouce.Checked == true)
                {
                    sendStateFlag = false;
                }
                else
                    sendStateFlag = true;
            }
            else
            {
                MessageBox.Show("لطفا اطلاعات را کامل کنید");
                ThreadData_Load(null, null);
            }

            Thread T = new Thread
            {
                description = txtDescription.Text,
                threadType = txtThreadType.Text,
                weight = float.Parse(txtWeight.Text),
                time = txtDoneTime.Value,
                sendState = sendStateFlag,
                amount = int.Parse(txtAmount.Text),
                price = float.Parse(txtPrice.Text),
                person = txtRSPerson.Text
            };

            if (txtId.Visible == true)
            {
                T.Update(int.Parse(txtId.Text));
                MessageBox.Show("اطلاعات با موفقیت به روزرسانی شد.");
                ThreadStock da = new ThreadStock();
                da.Show();
                Hide();
            }
            else
            {
                T.Add();
                MessageBox.Show("اطلاعات با موفقیت ذخیره شد.");
                ThreadStock da = new ThreadStock();
                da.Show();
                Hide();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ThreadStock frm = new ThreadStock();
            frm.Show();
            Hide();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Thread T = new Thread();
            var dialogResult = MessageBox.Show("مطمئنید؟", "هشدار", MessageBoxButtons.YesNoCancel);
            if (dialogResult == DialogResult.Yes)
            {
                T.DeleteThread(int.Parse(txtId.Text));
                MessageBox.Show("داده با موفقیت حذف شد");
                ThreadData frm = new ThreadData();
                frm.Show();
                Hide();
            }
        }
    }
}
