﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace textileWarehouse
{
    public partial class SumAllThread : Form
    {
        public SumAllThread()
        {
            InitializeComponent();
        }

        private void SumAllThread_Load(object sender, EventArgs e)
        {
            Thread T = new Thread();
            dataGridView1.DataSource = T.SelectAllThreadSum(); 
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            ThreadStock threadStock = new ThreadStock();
            threadStock.Show();
            Hide();
        }
    }
}
