﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace textileWarehouse
{
    public partial class Sum : Form
    {
        public int MachineNumber;
        public DateTime datetime;
        public Sum(int machineNumber, DateTime DT)
        {
            MachineNumber = machineNumber;
            datetime = DT;
            InitializeComponent();
        }

        private void Sum_Load(object sender, EventArgs e)
        {
            BL.Cloth C = new BL.Cloth();
            C.sendState = true;
            dataGridViewSend.DataSource = C.SumThreadWeight(datetime, MachineNumber);
            C.sendState = false;
            dataGridViewProduce.DataSource = C.SumThreadWeight(datetime, MachineNumber);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            ClothStock frm = new ClothStock();
            frm.Show();
            Hide();
        }
    }
}
