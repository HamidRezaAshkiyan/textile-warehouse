﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using BL;

namespace textileWarehouse
{
    public partial class ClothStock : Form
    {
        public int machineNumber = 1;

        public ClothStock()
        {
            InitializeComponent();
        }

        private void ClothStock_Load(object sender, EventArgs e)
        {
            Machine M = new Machine();
            Cloth C = new Cloth();
            DateTime DT = new DateTime();
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn();
            dt.Columns.Add("date");
            DT = dateTimePicker1.Value;

            machineStateGroup.Text = $"ماشین شماره {machineNumber}";

            dataGridViewProdouce.DataSource = C.SelectDayCloth(false, machineNumber, DT);
            dataGridViewSend.DataSource = C.SelectDayCloth(true, machineNumber, DT);

            // page button toggle 
            if (machineNumber == 1)
            {
                btnNextPage.Visible = true;
                btnPreviousPage.Visible = false;
            }
            else if (!M.IsMachineAvailable((machineNumber + 1)))
            {
                btnNextPage.Visible = false;
                btnPreviousPage.Visible = true;
            }
            else
            {
                btnNextPage.Visible = true;
                btnPreviousPage.Visible = true;
            }

            //sum section
            sumProduce.Text = C.SumMonthWeight(DT, machineNumber, false);
            sumSend.Text = C.SumMonthWeight(DT, machineNumber, true);
        }
        
        private void btnPreviousPage_Click(object sender, EventArgs e)
        {
            machineNumber--;
            ClothStock_Load(null, null);
        }
        private void btnNextPage_Click(object sender, EventArgs e)
        {
            machineNumber++;
            ClothStock_Load(null, null);
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Main frm = new Main();
            frm.Show();
            Hide();
        }

        private void btnSum_Click(object sender, EventArgs e)
        {
            Sum frm = new Sum(machineNumber, dateTimePicker1.Value);
            frm.Show();
            Hide();
        }

        private void btnAddData_Click(object sender, EventArgs e)
        {
            AddThreadData frm = new AddThreadData(true);
            frm.Show();
            Hide();
        }

        private void dateTimePicker_ValueChanged(object sender, EventArgs e) => ClothStock_Load(null, null);
    }
}
