﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using textileWarehouse;
using BL;

namespace textileWarehouse
{
    public partial class DailyProduce : Form
    {
        public int pageNumber = 1;
        public int lastMachineNumber = 2; //change to the default machine amount

        public DailyProduce()
        {
            InitializeComponent();
        }

        private void DailyProduce_Load(object sender, EventArgs e)
        {
            Cloth C = new Cloth();
            Machine M = new Machine();
            DateTime DT = new DateTime();
            DT = dateTimePicker1.Value;

            if (M.IsMachineAvailable(pageNumber * 2 - 1))
            {
                dataGridView1.DataSource = C.SelectCloth((pageNumber * 2 - 1), DT, false);
                btnEditMachineOne.Text = $"ویرایش ماشین {pageNumber * 2 - 1}";
                
                sumFirstMachine.Text = C.SumWeight(DT, pageNumber * 2 - 1);
                sumFirstMachineLable.Text = $"جمع تولید ماشین {pageNumber * 2 - 1}:";
            }

            if (M.IsMachineAvailable(pageNumber * 2))
            {
                dataGridView2.DataSource = C.SelectCloth((pageNumber * 2), DT, false);
                btnEditMachineTwo.Text = $"ویرایش ماشین {pageNumber * 2}";

                dataGridView2.Visible = true;
                btnEditMachineTwo.Visible = true;
                sumSecondMachineLable.Visible = true;
                sumSecondMachine.Visible = true;
                sumSecondMachine.Text = C.SumWeight(DT, pageNumber * 2);
                sumSecondMachineLable.Text = $"جمع تولید ماشین {pageNumber * 2}:";
            }
            else
            {
                dataGridView2.Visible = false;
                btnEditMachineTwo.Visible = false;
                sumSecondMachineLable.Visible = false;
                sumSecondMachine.Visible = false;
            }

            dataGridViewSum.DataSource = C.SumThreadWeight(DT, false);

            // page button toggle 
            if (pageNumber == 1)
            {
                btnNextPage.Visible = true;
                btnPreviousPage.Visible = false;
            }
            else if (!M.IsMachineAvailable((pageNumber + 1) * 2 - 1))
            {
                btnNextPage.Visible = false;
                btnPreviousPage.Visible = true;
            }
            else
            {
                btnNextPage.Visible = true;
                btnPreviousPage.Visible = true;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e) => DailyProduce_Load(null, null);

        private void btnAddMachine_Click(object sender, EventArgs e)
        {
            var dialogResult = MessageBox.Show("ایا تمایل به اضافه کردن ماشین دیگری دارید؟", "هشدار", MessageBoxButtons.YesNoCancel);
            if (dialogResult == DialogResult.Yes)
            {
                Machine M = new Machine();
                M.Add();
                lastMachineNumber++;
                MessageBox.Show(".ماشین جدید اضافه شد");
            }
        }

        private void btnAddData_Click(object sender, EventArgs e)
        {
            AddThreadData frm = new AddThreadData(false);
            frm.Show();
            Hide();
        }

        private void btnEditMachineOne_Click(object sender, EventArgs e)
        {
            Cloth C = new Cloth();
            DataTable dt = new DataTable();
            AddThreadData frm = new AddThreadData("ویرایش", false);
           
            string id = dataGridView1[0, dataGridView1.CurrentRow.Index].Value.ToString(); // for getting id
            dt = C.SelectOnePerformance(int.Parse(id));

            frm.dateTimePicker1.Value = DateTime.Parse(dt.Rows[0]["doneTime"].ToString());
            frm.txtDoneTime.Value = DateTime.Parse(dt.Rows[0]["doneTime"].ToString());
            frm.txtMachineNumber.Text = dt.Rows[0]["machineNumber"].ToString();
            frm.sendStateFlag = bool.Parse(dt.Rows[0]["sendState"].ToString());
            frm.txtTextureType.Text = dt.Rows[0]["textureType"].ToString();
            frm.txtThreadType.Text = dt.Rows[0]["threadType"].ToString();
            frm.txtWeight.Text = dt.Rows[0]["weight"].ToString();
            frm.txtAmount.Text = dt.Rows[0]["amount"].ToString();
            frm.txtId.Text = dt.Rows[0]["id"].ToString();
            frm.txtId.ReadOnly = true;
            frm.txtId.Visible = true;
            frm.id.Visible = true;

            frm.Show();
            Hide();
        }

        private void btnEditMachineTwo_Click(object sender, EventArgs e)
        {
            Cloth C = new Cloth();
            DataTable dt = new DataTable();
            AddThreadData frm = new AddThreadData("ویرایش", false);
            

            string id = dataGridView2[0, dataGridView2.CurrentRow.Index].Value.ToString(); // for getting id
            dt = C.SelectOnePerformance(int.Parse(id));

            frm.dateTimePicker1.Value = DateTime.Parse(dt.Rows[0]["doneTime"].ToString());
            frm.txtDoneTime.Value = DateTime.Parse(dt.Rows[0]["doneTime"].ToString());
            frm.txtMachineNumber.Text = dt.Rows[0]["machineNumber"].ToString();
            frm.txtTextureType.Text = dt.Rows[0]["textureType"].ToString();
            frm.txtThreadType.Text = dt.Rows[0]["threadType"].ToString();
            frm.txtWeight.Text = dt.Rows[0]["weight"].ToString();
            frm.txtAmount.Text = dt.Rows[0]["amount"].ToString();
            frm.txtId.Text = dt.Rows[0]["id"].ToString();
            frm.txtId.ReadOnly = true;
            frm.txtId.ReadOnly = true;
            frm.txtId.Visible = true;
            frm.id.Visible = true;

            frm.Show();
            Hide();
        }

        private void btnPreviousPage_Click(object sender, EventArgs e)
        {
            pageNumber--;
            DailyProduce_Load(null, null);
        }

        private void btnNextPage_Click(object sender, EventArgs e)
        {
            pageNumber++;
            DailyProduce_Load(null, null);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Main frm = new Main();
            frm.Show();
            Hide();
        }
    }
}
