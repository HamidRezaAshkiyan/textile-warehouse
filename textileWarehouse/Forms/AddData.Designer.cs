﻿namespace textileWarehouse
{
    partial class AddThreadData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.threadType = new System.Windows.Forms.Label();
            this.textureType = new System.Windows.Forms.Label();
            this.doneTime = new System.Windows.Forms.Label();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.weight = new System.Windows.Forms.Label();
            this.txtMachineNumber = new System.Windows.Forms.TextBox();
            this.machineNumber = new System.Windows.Forms.Label();
            this.txtDoneTime = new System.Windows.Forms.DateTimePicker();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtId = new System.Windows.Forms.TextBox();
            this.id = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.amount = new System.Windows.Forms.Label();
            this.radioButtonSend = new System.Windows.Forms.RadioButton();
            this.radioButtonProdouce = new System.Windows.Forms.RadioButton();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtThreadType = new System.Windows.Forms.ComboBox();
            this.txtTextureType = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // threadType
            // 
            this.threadType.AutoSize = true;
            this.threadType.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.threadType.Location = new System.Drawing.Point(283, 152);
            this.threadType.Name = "threadType";
            this.threadType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.threadType.Size = new System.Drawing.Size(47, 21);
            this.threadType.TabIndex = 5;
            this.threadType.Text = "نوع نخ";
            // 
            // textureType
            // 
            this.textureType.AutoSize = true;
            this.textureType.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.textureType.Location = new System.Drawing.Point(283, 200);
            this.textureType.Name = "textureType";
            this.textureType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textureType.Size = new System.Drawing.Size(59, 21);
            this.textureType.TabIndex = 6;
            this.textureType.Text = "نوع بافت";
            // 
            // doneTime
            // 
            this.doneTime.AutoSize = true;
            this.doneTime.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.doneTime.Location = new System.Drawing.Point(278, 34);
            this.doneTime.Name = "doneTime";
            this.doneTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.doneTime.Size = new System.Drawing.Size(64, 21);
            this.doneTime.TabIndex = 9;
            this.doneTime.Text = "زمان تولید";
            // 
            // txtWeight
            // 
            this.txtWeight.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtWeight.Location = new System.Drawing.Point(44, 242);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(200, 25);
            this.txtWeight.TabIndex = 2;
            // 
            // weight
            // 
            this.weight.AutoSize = true;
            this.weight.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.weight.Location = new System.Drawing.Point(283, 244);
            this.weight.Name = "weight";
            this.weight.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.weight.Size = new System.Drawing.Size(30, 21);
            this.weight.TabIndex = 7;
            this.weight.Text = "وزن";
            // 
            // txtMachineNumber
            // 
            this.txtMachineNumber.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtMachineNumber.Location = new System.Drawing.Point(44, 288);
            this.txtMachineNumber.Name = "txtMachineNumber";
            this.txtMachineNumber.Size = new System.Drawing.Size(200, 25);
            this.txtMachineNumber.TabIndex = 3;
            // 
            // machineNumber
            // 
            this.machineNumber.AutoSize = true;
            this.machineNumber.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.machineNumber.Location = new System.Drawing.Point(283, 291);
            this.machineNumber.Name = "machineNumber";
            this.machineNumber.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.machineNumber.Size = new System.Drawing.Size(79, 21);
            this.machineNumber.TabIndex = 8;
            this.machineNumber.Text = "شماره ماشین";
            // 
            // txtDoneTime
            // 
            this.txtDoneTime.CalendarFont = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtDoneTime.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtDoneTime.Location = new System.Drawing.Point(44, 61);
            this.txtDoneTime.Name = "txtDoneTime";
            this.txtDoneTime.Size = new System.Drawing.Size(200, 28);
            this.txtDoneTime.TabIndex = 11;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnSubmit.Location = new System.Drawing.Point(278, 412);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(83, 25);
            this.btnSubmit.TabIndex = 7;
            this.btnSubmit.Text = "ثبت";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // txtId
            // 
            this.txtId.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtId.Location = new System.Drawing.Point(44, 334);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(200, 25);
            this.txtId.TabIndex = 4;
            this.txtId.Visible = false;
            // 
            // id
            // 
            this.id.AutoSize = true;
            this.id.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.id.Location = new System.Drawing.Point(283, 334);
            this.id.Name = "id";
            this.id.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.id.Size = new System.Drawing.Size(23, 21);
            this.id.TabIndex = 12;
            this.id.Text = "کد";
            this.id.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnCancel.Location = new System.Drawing.Point(44, 412);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(83, 25);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "انصراف";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtAmount.Location = new System.Drawing.Point(44, 104);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtAmount.Size = new System.Drawing.Size(200, 25);
            this.txtAmount.TabIndex = 12;
            this.txtAmount.Text = "1";
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // amount
            // 
            this.amount.AutoSize = true;
            this.amount.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.amount.Location = new System.Drawing.Point(283, 105);
            this.amount.Name = "amount";
            this.amount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.amount.Size = new System.Drawing.Size(37, 21);
            this.amount.TabIndex = 15;
            this.amount.Text = "تعداد";
            // 
            // radioButtonSend
            // 
            this.radioButtonSend.AutoSize = true;
            this.radioButtonSend.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.radioButtonSend.Location = new System.Drawing.Point(188, 372);
            this.radioButtonSend.Name = "radioButtonSend";
            this.radioButtonSend.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButtonSend.Size = new System.Drawing.Size(56, 25);
            this.radioButtonSend.TabIndex = 5;
            this.radioButtonSend.TabStop = true;
            this.radioButtonSend.Text = "ارسال";
            this.radioButtonSend.UseVisualStyleBackColor = true;
            // 
            // radioButtonProdouce
            // 
            this.radioButtonProdouce.AutoSize = true;
            this.radioButtonProdouce.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.radioButtonProdouce.Location = new System.Drawing.Point(73, 372);
            this.radioButtonProdouce.Name = "radioButtonProdouce";
            this.radioButtonProdouce.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioButtonProdouce.Size = new System.Drawing.Size(54, 25);
            this.radioButtonProdouce.TabIndex = 6;
            this.radioButtonProdouce.TabStop = true;
            this.radioButtonProdouce.Text = "تولید";
            this.radioButtonProdouce.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarFont = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dateTimePicker1.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.dateTimePicker1.Location = new System.Drawing.Point(44, 27);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 28);
            this.dateTimePicker1.TabIndex = 10;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("IRANSans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnDelete.Location = new System.Drawing.Point(161, 412);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(83, 25);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "حذف";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtThreadType
            // 
            this.txtThreadType.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtThreadType.FormattingEnabled = true;
            this.txtThreadType.Items.AddRange(new object[] {
            "تجارت",
            "البرز",
            "صادرات"});
            this.txtThreadType.Location = new System.Drawing.Point(44, 150);
            this.txtThreadType.Name = "txtThreadType";
            this.txtThreadType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtThreadType.Size = new System.Drawing.Size(200, 25);
            this.txtThreadType.TabIndex = 0;
            // 
            // txtTextureType
            // 
            this.txtTextureType.Font = new System.Drawing.Font("IRANSans", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtTextureType.FormattingEnabled = true;
            this.txtTextureType.Items.AddRange(new object[] {
            "خطی",
            "دورو"});
            this.txtTextureType.Location = new System.Drawing.Point(44, 196);
            this.txtTextureType.Name = "txtTextureType";
            this.txtTextureType.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTextureType.Size = new System.Drawing.Size(200, 25);
            this.txtTextureType.TabIndex = 1;
            // 
            // AddThreadData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 470);
            this.Controls.Add(this.txtTextureType);
            this.Controls.Add(this.txtThreadType);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.radioButtonProdouce);
            this.Controls.Add(this.radioButtonSend);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.amount);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.id);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.txtDoneTime);
            this.Controls.Add(this.txtMachineNumber);
            this.Controls.Add(this.machineNumber);
            this.Controls.Add(this.txtWeight);
            this.Controls.Add(this.weight);
            this.Controls.Add(this.doneTime);
            this.Controls.Add(this.textureType);
            this.Controls.Add(this.threadType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddThreadData";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddData";
            this.Load += new System.EventHandler(this.AddData_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Button btnSubmit;
        public System.Windows.Forms.TextBox txtWeight;
        public System.Windows.Forms.TextBox txtMachineNumber;
        public System.Windows.Forms.DateTimePicker txtDoneTime;
        public System.Windows.Forms.Label threadType;
        public System.Windows.Forms.Label textureType;
        public System.Windows.Forms.Label doneTime;
        public System.Windows.Forms.Label weight;
        public System.Windows.Forms.Label machineNumber;
        public System.Windows.Forms.TextBox txtId;
        public System.Windows.Forms.Label id;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.TextBox txtAmount;
        public System.Windows.Forms.Label amount;
        private System.Windows.Forms.RadioButton radioButtonSend;
        private System.Windows.Forms.RadioButton radioButtonProdouce;
        public System.Windows.Forms.DateTimePicker dateTimePicker1;
        public System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.ComboBox txtThreadType;
        public System.Windows.Forms.ComboBox txtTextureType;
    }
}