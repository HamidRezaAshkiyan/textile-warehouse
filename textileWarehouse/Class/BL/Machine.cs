﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace BL
{
    class Machine : DA.DataAccessLayer
    {
        public int machineNumber = 1;

        public void Add() //Add one machine to table
        {
            string querySelect = "SELECT * FROM Machine WHERE ID = (SELECT MAX(ID) FROM Machine)"; //To find the Max machine#
            DataTable dt = Select(querySelect);

            string query = "insert into Machine (machineNumber) values({0})";
            query = string.Format(query, int.Parse(dt.Rows[0][1].ToString()) + 1);

            base.Connect();
            base.DoCommand(query);
            base.Disconnect();
        }

        public DataTable Select()
        {
            string query = "select * from Machine";
            base.Connect();
            DataTable dt = new DataTable();
            dt = base.Select(query);
            base.Disconnect();

            return dt;
        }

        public bool IsMachineAvailable(int machineNumber)
        {
            string query = "select 1 from Machine where machineNumber = {0}";
            query = string.Format(query, machineNumber);

            base.Connect();
            DataTable dt = base.Select(query);
            base.Disconnect();

            if (dt.Rows.Count > 0)
                return true;
            return false;
        }
    }
}
