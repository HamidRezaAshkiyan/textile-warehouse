﻿using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Forms;
using DA;


namespace BL
{
    class Cloth : DataAccessLayer
    {
        public DateTime doneTime;
        public string textureType;
        public string threadType;
        public int machineNumber;
        public bool sendState;
        public float weight;
        public int amount;
        

        public void Add() // insert into cloth table
        {
            string query = "insert into Cloth (sendState, amount, textureType, threadType, doneTime, weight, machineNumber) values('{0}', {1}, N'{2}', N'{3}', convert(datetime, N'{4}', 5), {5}, '{6}')";
            query = string.Format(query, sendState, amount, textureType, threadType, doneTime.ToString("dd/MM/yy hh:mm:ss tt", CultureInfo.CurrentUICulture), weight.ToString(CultureInfo.InvariantCulture), machineNumber);

            Connect();
            DoCommand(query);
            Disconnect();
        }

        public void Update(int id) //for update information of cloth table
        {
            string query = "update Cloth set doneTime = convert(datetime, N'{0}', 5), textureType = N'{1}', threadType = N'{2}', weight = {3}, machineNumber = {4}, amount = {5}, sendState = '{6}' where id = {7}";
            query = string.Format(query, doneTime.ToString("dd/MM/yy hh:mm:ss tt", CultureInfo.CurrentUICulture), textureType, threadType, weight.ToString(CultureInfo.InvariantCulture), machineNumber, amount, sendState, id);

            Connect();
            DoCommand(query);
            Disconnect();
        }

        public DataTable SelectDayCloth(bool sendState, int machineNumber, DateTime DT) // select all data for a machine in a Day
        {
            DataTable dt = new DataTable();
            string query = "select *, convert(date, doneTime) as date from Cloth where sendState = '{0}' and machineNumber = '{1}' and CONVERT(date, doneTime) = CONVERT(date, '{2}')";
            query = string.Format(query, sendState, machineNumber, DT.ToString("yyyy/MM/dd", CultureInfo.CurrentUICulture));

            Connect();
            dt = Select(query);
            Disconnect();

            dt.Columns.Remove("doneTime");
            dt.Columns.Remove("machineNumber");
            dt.Columns.Remove("sendState");
            return dt;
        }

        public DataTable SelectCloth(int machineNumber, DateTime DT, bool sendState) // for selecting all data for a machine (one day)
        {
            DataTable dt = new DataTable();
            string query = "select * from Cloth where machineNumber = '{0}' and CONVERT(date, doneTime) = CONVERT(date, '{1}') and sendState = '{2}'";
            query = string.Format(query, machineNumber, DT.ToString("yyyy/MM/dd", CultureInfo.CurrentUICulture), sendState);

            Connect();
            dt = Select(query);
            Disconnect();

            dt.Columns.Remove("machineNumber");
            dt.Columns.Remove("sendState");
            dt.Columns.Remove("amount");
            return dt;
        }

        public DataTable SelectOnePerformance(int ID) // select one unique record based on id // for edit
        {
            DataTable dt = new DataTable();
            string query = "select * from Cloth where id = {0}";
            query = String.Format(query, ID);

            Connect();
            dt = Select(query);
            Disconnect();

            return dt;
        }

        public string SumWeight(DateTime DT, int machineNumber) // for getting sum of one machine in one day
        {
            DataTable dt = new DataTable();
            string query = "SELECT SUM(weight) FROM Cloth WHERE CONVERT(date, doneTime) = CONVERT(date, '{0}') and machineNumber = {1}";
            query = string.Format(query, DT.ToString("yyyy/MM/dd", CultureInfo.CurrentUICulture), machineNumber);

            Connect();
            dt = Select(query);
            Disconnect();

            return dt.Rows[0][0].ToString();
        }

        public string SumMonthWeight(DateTime DT, int machineNumber, bool sendState) // for getting sum of one machine in one day
        {
            DataTable dt = new DataTable();

            string query = $"SELECT doneTime, SUM(weight) FROM Cloth WHERE machineNumber = {machineNumber} and sendState = '{sendState}' GROUP BY doneTime";
            Connect();
            dt = Select(query);
            Disconnect();


            int rowsCount = dt.Rows.Count;
            for (int i = 0; i < rowsCount; i++)
            {
                DateTime Date = (DateTime)dt.Rows[i]["doneTime"];

                int month = int.Parse(Date.ToString().Substring(3, 2));
                int DTmonth = int.Parse(DT.ToString().Substring(3, 2));
                int year = int.Parse(Date.ToString().Substring(6, 4));
                int DTyear = int.Parse(DT.ToString().Substring(6, 4));

                if (month != DTmonth || year != DTyear)
                    dt.Rows.Remove(dt.Rows[i]);
            }
            dt.Columns.Remove("doneTime");

            if (dt.Rows.Count > 0)
                return dt.Rows[0][0].ToString();
            else return null;

        }

        public DataTable SumThreadWeight(DateTime DT, bool sendState) //return all thread with their sum weight
        {
            DataTable dt = new DataTable();

            string query = $"SELECT threadType, SUM(weight) as weight, doneTime FROM Cloth WHERE sendState = '{sendState}'  GROUP BY threadType, doneTime";
            Connect();
            dt = Select(query);
            Disconnect();
            

            dt.Columns[2].DataType = typeof(DateTime);
            int rowsCount = dt.Rows.Count;
            for (int i = 0; i < rowsCount; i++)
            {
                DateTime Date = (DateTime) dt.Rows[i]["doneTime"];

                int month = int.Parse(Date.ToString().Substring(3, 2));
                int DTmonth = int.Parse(DT.ToString().Substring(3, 2));

                if (month != DTmonth)
                    dt.Rows.Remove(dt.Rows[i]);
            }

            dt.Columns.Remove("doneTime");
            return dt;
        }

        public DataTable SumThreadWeight(DateTime DT, int MN) //return all thread with their sum weight for special machine
        {
            DataTable dt = new DataTable();
            machineNumber = MN;
            string query = "SELECT threadType, SUM(weight) as weight FROM Cloth WHERE CONVERT(date, doneTime) = CONVERT(date, '{0}') and machineNumber = '{1}' and sendState = '{2}' GROUP BY threadType";
            query = string.Format(query, DT.ToString("yyyy/MM/dd", CultureInfo.CurrentUICulture), machineNumber, sendState);

            Connect();
            dt = Select(query);
            Disconnect();

            return dt;
        }

        public DataTable SumAllMachineThread(DateTime DT) //return all thread with their sum weight
        {
            DataTable dt = new DataTable();
            
            string query = $"SELECT threadType, doneTime, SUM(weight) as weight FROM Cloth WHERE sendState = '{false}' GROUP BY threadType, doneTime";
            Connect();
            dt = Select(query);
            Disconnect();

            int rowsCount = dt.Rows.Count;
            for (int i = 0; i < rowsCount; i++)
            {
                DateTime Date = (DateTime)dt.Rows[i]["doneTime"];

                int month = int.Parse(Date.ToString().Substring(3, 2));
                int DTmonth = int.Parse(DT.ToString().Substring(3, 2));
                int year = int.Parse(Date.ToString().Substring(6, 4));
                int DTyear = int.Parse(DT.ToString().Substring(6, 4));

                if (month != DTmonth || year != DTyear)
                    dt.Rows.Remove(dt.Rows[i]);
            }

            dt.Columns.Remove("doneTime");
            return dt;
        }

        public void DeleteCloth(int id)
        {
            string query = $"delete from Cloth where id = {id}";

            Connect();
            DoCommand(query);
            Disconnect();
        }
    }
}


 

        

        

        