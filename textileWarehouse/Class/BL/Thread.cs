﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Globalization;
using DA;

namespace BL
{
    class Thread : DataAccessLayer
    {
        public DateTime time;
        public string threadType;
        public string description;
        public string person;
        public float price;
        public bool sendState;
        public float weight;
        public int amount;

        public void Add() // insert into thread table
        {
            string query = "insert into Thread (sendState, amount, threadType, time, weight, person, price, description) values('{0}', {1}, N'{2}', convert(datetime, N'{3}', 5), {4}, N'{5}', {6}, N'{7}')";
            query = string.Format(query, sendState, amount, threadType, time.ToString("dd/MM/yy hh:mm:ss tt", CultureInfo.CurrentUICulture), weight.ToString(CultureInfo.InvariantCulture), person, price.ToString(CultureInfo.InvariantCulture), description);

            Connect();
            DoCommand(query);
            Disconnect();
        }

        public void Update(int id) //for update information of thread table
        {
            string query = "update Thread set time = convert(datetime, N'{0}', 5), threadType = N'{1}', weight = {2}, amount = {3}, sendState = '{4}', price = {5}, person = N'{6}', description = N'{7}' where id = {8}";
            query = string.Format(query, time.ToString("dd/MM/yy hh:mm:ss tt", CultureInfo.CurrentUICulture), threadType, weight.ToString(CultureInfo.InvariantCulture), amount, sendState, price.ToString(CultureInfo.InvariantCulture), person,description, id);
            
            Connect();
            DoCommand(query);
            Disconnect();
        }

        internal DataTable SelectOneThread(int id)
        {
            DataTable dt = new DataTable();
            string query = "select * from Thread where id = {0}";
            query = String.Format(query, id);

            Connect();
            dt = Select(query);
            Disconnect();

            return dt;
        }

        public DataTable SelectYearThread(bool sendState, DateTime DT) // select all data for a machine in a year
        {
            DataTable dt = new DataTable();
            string query = $"select *, convert(date, time) as date from Thread where sendState = '{sendState}'";

            Connect();
            dt = Select(query);
            Disconnect();
            
            int rowsCount = dt.Rows.Count;
            for (int i = 0; i < rowsCount; i++)
            {
                DateTime Date = (DateTime)dt.Rows[i]["time"];
                
                int year = int.Parse(Date.ToString().Substring(6, 4));
                int DTYear = int.Parse(DT.ToString().Substring(6, 4));

                if (year != DTYear)
                    dt.Rows.Remove(dt.Rows[i]);
            }

            dt.Columns.Remove("time");
            dt.Columns.Remove("sendState");
            return dt;
        }

        public DataTable SelectAllThreadSum() // special method that has one class from cloth 
        {
            DataTable dt = new DataTable();
            string query = "select threadType, sum(weight) - ((select sum(weight) from Thread where sendState = 'true') + (select sum(weight) from cloth where sendState = 'false')) as weight from Thread where sendState = 'false' group by threadType";

            Connect();
            dt = Select(query);
            Disconnect();
            
            return dt;
        }

        internal void DeleteThread(int id)
        {
            string query = $"delete from Thread where id = {id}";

            Connect();
            DoCommand(query);
            Disconnect();
        }
    }
}
