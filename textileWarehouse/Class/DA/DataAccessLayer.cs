﻿using System.Data;
using System.Data.SqlClient;

namespace DA
{
    public class DataAccessLayer
    {
        SqlConnection con;
        SqlCommand cmd;
        SqlDataAdapter da;
        DataTable dt;

        public DataAccessLayer()
        {
            con = new SqlConnection();
            cmd = new SqlCommand();
            da = new SqlDataAdapter();
            cmd.Connection = con;
            da.SelectCommand = cmd;
        }

        public void Connect() // method for connecting to database
        {
            con.ConnectionString = $@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\DB.mdf;Integrated Security=True";
            con.Open();
        }

        public void Disconnect() // method for disconnecting from database
        {
            con.Close();
        }

        public DataTable Select(string query) // method for database select query
        {
            cmd.CommandText = query;
            dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        
        public void DoCommand(string query) // method for other queries
        {
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
        }
    }
}
